param (
    [Parameter(Mandatory=$true)][string]$source,
    [Parameter(Mandatory=$true)][string]$thedestinations,
    [Parameter(Mandatory=$false)][string]$filetype
)



if(!$filetype)
{
    $filetype = "dat"
}


$thefiles = Get-ChildItem -Path $source -Filter *.$($filetype) 



#get the destinations by splitting string to array on ">"
$destination = $thedestinations -split ">"

try
{
    foreach($file in $thefiles)
    {
        #fix the dot problem
        $zipname = $file.Name.substring(0,$file.Name.LastIndexOf('.')).Replace('.','_').Replace(' ','-').Replace('[','').Replace(']','')
        
        echo "name of zip: " $zipname
        Compress-Archive -Path $file.FullName -DestinationPath $source/$($zipname) -CompressionLevel Optimal

        foreach($d in $destination)
        {
            echo "destination: " $d
            # get free space
            $dest_drive = $d.substring(0,2)
            $comp_name = Get-WmiObject -Class Win32_OperatingSystem -ComputerName . | Select-Object -Property PSComputerName
            $storage_space = Get-WMIObject Win32_Logicaldisk -filter "deviceid='$dest_drive'" -ComputerName $comp_name.PSComputerName
            
            #echo "Free: " $storage_space.FreeSpace

            $filesize_temp =  Get-ItemProperty $source/$($zipname).zip -Name Length
            $file_size = $filesize_temp.length * 2 #(to much work to get file size on disk)

            if($file_size -gt $storage_space.FreeSpace)
            {
                $dest_files =  Get-ChildItem -Path $d | Sort-Object -Property LastWriteTime
                foreach ($dest_file in $dest_files)
                {
                    Remove-Item -Path $d/$dest_file
                    echo "removed file: " $d/$dest_file
                    $storage_space = Get-WMIObject Win32_Logicaldisk -filter "deviceid='$dest_drive'" -ComputerName $comp_name.PSComputerName
                    if($file_size -lt $storage_space.FreeSpace)
                    {
                        break
                    }
                }

            }

            Copy-Item -Path $source/$($zipname).zip -Destination $d/$($zipname).zip
            echo "moved zip file: " $d/$($zipname).zip
        }

        #remove every file from source folder after it have been moved to destination
        Remove-Item -Path $file.FullName
        Remove-Item -Path $source/$($zipname).zip


    }

}
catch
{
    $logfilename = "/shrinklog.txt"
    
    $logfile = $source + $logfilename
    echo "******************************" >> $logfile
    Get-Date -UFormat %c >> $logfile
    echo Source:$($source) Destination:$($thedestinations) Extention:$($filetype) >> $logfile
    $_.Exception.GetType().FullName, $_.Exception.Message >> $logfile

    echo Source:$($source) Destination:$($thedestinations) Extention:$($filetype)
    echo $_.Exception.GetType().FullName, $_.Exception.Message
} 